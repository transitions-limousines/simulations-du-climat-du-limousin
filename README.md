# Simulations du climat du Limousin

Ce dépôt regroupe les fichiers de données, espaces de travail et éléments de code utilisés dans le cadre du dossier technique "Simulations du climat du Limousin (RCP4.5 et 8.5, horizons H2 et H3)" élaboré par Transitions Limousines en 2023. L'article principal présentant ce dossier peut être consulté [via ce lien](https://transitions-limousines.org/dossier-technique-simulations-du-climat-du-limousin-a-horizon-milieu-et-fin-de-siecle).

## Auteurs et citations

Pour citer ce travail, merci d'indiquer :

- la mention : "Dossier technique "Simulations du climat du Limousin à horizon milieu et fin de siècle publié au printemps 2023 par Transitions Limousines (auteurs : Eric Boniface, Chloé Legrand, Nicolas Picard)"
- le lien vers l'article principal : [lien](https://transitions-limousines.org/dossier-technique-simulations-du-climat-du-limousin-a-horizon-milieu-et-fin-de-siecle)

## License

Le dossier technique "Simulations du climat du Limousin (RCP4.5 et 8.5, horizons H2 et H3)" élaboré par Transitions Limousines est versé au domaine public, à l'exception des cartes et graphiques qui sont distribués sous lience CC BY-ND 4.0 (et ne peuvent donc pas être modifiés sans accord exprès des auteurs).
